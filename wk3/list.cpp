#include "list.h"

List::List() {
    List::InitNodes();
}

void List::InitNodes() {
    head = NULL;
    tail = NULL;
}

void List::disp() {
    node *tmp;
    tmp = head;
    printf("** BEGIN\n");
    while(tmp != NULL) {
        printf("%5d\n", tmp->data);
        tmp = tmp->next;
    }
    printf("** END\n");
}

void List::addNode(int d) {
    node *tmp = new node;
    tmp->data = d;
    tmp->next = NULL;

    if(head == NULL) {
        head = tmp;
        tail = tmp;
    }
    else {
        tail->next = tmp;
        tail = tail->next;
    }
}

void List::addNodeToStart(int d) {
    node *tmp = new node;
    tmp -> data = d;
    tmp -> next = head;
    head = tmp;
}

void List::delNode() {
    node *tmp;
    tmp = head;
    while(tmp) {
        if(tmp -> next == tail) {
            free(tmp -> next);
            tail = tmp;
            tail -> next = NULL;
        }
        else {
            tmp = tmp -> next;
        }
    }
}

void List::delNodeFromStart() {
    node *tmp;
    tmp = head;
    head = head -> next;
    free(tmp);
}

void List::clear() {
    node *tmp;
    tmp = head;
    while(tmp) {
        tmp = tmp -> next;
        delNodeFromStart();
    } 
}