//list.h
#include <iostream>
#include <stdlib.h>

#ifndef list
#define list

struct node {
    int data;
    node *next;
};

class List {
    node *head, *tail;

    public:
        List();
        void InitNodes();
        void disp();
        void addNode(int d);
        void addNodeToStart(int d);
        void delNode();
        void delNodeFromStart();
        void clear();
};

#endif