#include "list.h"

int main() {
    List mylist;
    mylist.disp();
    mylist.addNode(1);
    mylist.disp();
    mylist.addNode(4);
    mylist.disp();
    mylist.addNode(8);
    mylist.disp();
    mylist.addNode(8);
    mylist.disp();
    mylist.addNodeToStart(8);
    mylist.disp();
    mylist.delNodeFromStart();
    mylist.disp();
    mylist.delNode();
    mylist.disp();
    mylist.addNode(9);
    mylist.disp();
    mylist.clear();
    mylist.disp();
    return 0;
}
